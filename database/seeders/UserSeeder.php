<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            [
                'id'=>0,
                'name'=>'Super',
                'posisi'=> 'Ketua',
                'username'=>'super',
                'email'=>'super@super.com',
                'is_admin'=>1,
                'password'=>bcrypt('bismillah2022'),
            ]
        ];

        User::insert($user);
    }
}
