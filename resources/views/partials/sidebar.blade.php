
<nav id="sidebarMenu"  class="col-md-3 col-lg-2 d-md-block bg-dark navbar-dark sidebar collapse">
    <div class="position-sticky pt-3" >

        <ul class="nav flex-column mt-2">

                <li class="nav-item ">
                    <a class="nav-link {{ Request::is('/')? 'active fw-bold' : ''  }}" aria-current="page" href="/">
                        <i class="bi bi-window-desktop"></i>
                        Dashboard
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link {{ Request::is('dashboard/data-anggota*')? 'active fw-bold' : ''  }}" aria-current="page" href="/dashboard/data-anggota">
                        <i class="bi bi-window-desktop"></i>
                        Data Anggota
                    </a>
                </li>

                <li class="nav-item ">
                    <a class="nav-link {{ Request::is('dashboard/myprofile*')? 'active fw-bold' : ''  }}" href="/dashboard/myprofile">
                        <i class="bi bi-table"></i>
                        My Profile
                    </a>
                </li>


        </ul>
    </div>
  </nav>
