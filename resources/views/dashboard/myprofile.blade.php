@extends('dashboard.index')

@section('css')

    <style type="text/css">
        .input{
            position: absolute;
            bottom: 0;
            top: 5%;
            left: 0;
            right: 0;
            height: 100%;
            opacity: 0.5;
            z-index: 10;
        }

        .overlay {
            position: absolute;
            bottom: 0;
            left: 0;
            right: 0;
            height: 70%;
            width: 150px;
            padding: 10px;
            opacity: 0;
            transition: .4s;
            cursor: pointer;
            overflow: hidden;

        }
        .text {
            color: white;
            position: relative;
            font-size: 0.9em;
            text-align: center;
            width: 150px;
            font-weight: bold;
            top: 50%;
            left: 50%;
            opacity: 1;
            transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
        }
        .img-profile{
            position: relative;
            display: inline-block;
        }


        .img-profile:hover .overlay{
            bottom: 8%;
            width: 150px;
            height: 150px;
            opacity: 1;
            background-color:rgba(0, 0, 0, 0.36);
            overflow: hidden;
            transition: .4s;

        }


        .label-photo{
            padding-bottom: 70px;
            padding-top: 70px;
            color: white;
            font-weight: bold;
            cursor: pointer;
        }

        .img-ava{
            object-fit: cover;
            border: 5px solid #a23234;
            border-left-style: none;
            border-right-style: none;
            border-top-style: none;
            padding: 10px;
        }
    </style>
@endsection

@section('content')
    <livewire:my-profile></livewire:my-profile>
@endsection
