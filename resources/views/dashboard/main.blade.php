@extends('dashboard.index')

@section('css')
    <style>
         span.rounded-md{
            display: none !important;
        }
    </style>
@endsection

@section('content')
    <livewire:users-index></livewire:users-index>

@endsection
