<div>
    <form wire:submit.prevent='store'>
        {{--  @csrf @method('PUT')  --}}
        {{--  <input type="hidden" name="iduser" >  --}}
        <div class="form-floating mb-3">
            <input wire:model='name' type="text" name="name" class="form-control @error('name') is-invalid @enderror" id="name"  >
            <label for="name">Nama</label>
            @error('name')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-floating mb-3">
            <input wire:model='username' type="text" name="username" class="form-control
             @error('username') is-invalid @enderror" id="username" >
            <label for="username">Username</label>
            @error('username')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-floating mb-3">
            <input wire:model='posisi' type="text" name="posisi" class="form-control
             @error('posisi') is-invalid @enderror" id="posisi"  >
            <label for="from">Posisi</label>
            @error('posisi')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-floating mb-3">
            <input wire:model='email' type="email" name="email" class="form-control
             @error('email') is-invalid @enderror" id="email"  >
            <label for="email">Email</label>
            @error('email')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-floating mb-3">
            <input wire:model='password' type="password" name="password" class="form-control
             @error('password') is-invalid @enderror" id="password"  >
            <label for="password">Password</label>
            @error('password')
                <div class="invalid-feedback">
                    {{ $message }}
                </div>
            @enderror
        </div>

        <div class="form-check">
            <input wire:model='is_admin' type="checkbox" name="is_admin" class="form-check-input" id="is_admin" >
            <label class="form-check-label" for="is_admin"><small>Administrator</small></label>
        </div>

        <div class="modal-footer">
            <button type="submit" class="btn btn-primary w-auto" data-bs-dismiss="modal">Create</button>
        </div>

    </form>
</div>

