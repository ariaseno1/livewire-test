<div>
    <h2 class="fs-2">Data Keanggotaan</h2>
    <hr>

    @include('partials.modal-add')

    <div class="row">
        <div class="col-6">
            <button type="button" class="btn btn-primary btn-sm my-2 w-auto" data-bs-toggle="modal" data-bs-target="#exampleModal">
                Tambah Data
            </button>
        </div>
        <div class="col-4">
            <div>
                @if(session() -> has ('success'))
                <div class="alert alert-success d-flex align-items-center" id="alert-success" role="alert">
                    <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:"><use xlink:href="#check-circle-fill"/></svg>
                    <div class="text-small">
                        {{ session('success') }}
                        <button type="button" class="me-0 btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                </div>
                @endif
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-6 my-2">
            <select wire:model="paginate" name="" id="" class="form-control  p-1 text-center w-auto">
                <option value="5">5</option>
                <option value="10">10</option>
                <option value="15">15</option>
            </select>
        </div>
        <div class="col-4">
            <input wire:model='search' class="form-control" type="search" name="" id="" placeholder="Search...">

        </div>
    </div>


    <div class="col-lg-10">
        <div class="table-responsive">
            <table class="table table-striped table">
                <tr style="background-color: rgb(214, 214, 214)">
                    <th>ID</th>
                    <th>Name</th>
                    <th>Posisi</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Admin</th>
                    <th>#</th>

                </tr>
                @foreach ( $users as $u )
                    <tr>
                        <td>{{ $u->id }}</td>
                        <td>{{ $u->name }}</td>
                        <td>{{ $u->posisi }}</td>
                        <td>{{ $u->username }}</td>
                        <td>{{ $u->email }}</td>
                        <td>{{ $u->is_admin? 'Yes': "No" }}</td>
                        <td>
                            <a class="btn btn-edit btn-sm" data-bs-toggle="modal" name="edit" data-bs-target="#modal-edit-{{ $u->id }}" style="border: 2px solid black">Edit</a>
                            |
                            <a data-bs-toggle="modal" class="btn btn-danger btn-sm" style="margin-left: -20px"  data-bs-target="#modal-delete-{{ $u->id }}" >Delete</a>
                        </td>
                    </tr>
                @endforeach

            </table>

            {{ $users->links() }}

        </div>
    </div>

</div>

