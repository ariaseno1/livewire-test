<div>
    <div class="main-content">
        <section class="section">
          <div class="section-header">
            <h1>My Profile</h1>
            <hr>
          </div>
          <div class="section-body">
            <div class="row mt-4">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <h4>Data Akun</h4>
                  </div>
                  <div class="card-body">
                    <div class="form-group mb-0 pb-0 row align-items-center">
                        <label for="name" class="form-control-label col-sm-3 text-md-right">Photo</label>

                    </div>
                    <div class="form-group row align-items-center mx-auto d-block img-profile" style="width: 170px">
                    </div>

                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Nama</label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            : {{ $userId->name }}
                        </div>
                    </div>
                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Posisi</label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            : {{ $userId->posisi }}
                        </div>
                    </div>

                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Username</label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            : {{ $userId->username }}
                        </div>
                    </div>

                    <div class="form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right">Email</label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            : {{ $userId->email }}
                        </div>
                    </div>


                    <div class="my-3 form-group row align-items-center">
                        <label for="site-title" class="form-control-label col-sm-3 text-md-right"></label>
                        <div class="col-sm-6 col-md-9 col-lg-6">
                            <a href="" class="btn btn-primary">Edit</a>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
</div>
