<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;
use PDO;

class UsersIndex extends Component
{
    use WithPagination;

    public $search;
    public $paginate = 5;

    protected $listeners = [
        'userCreated' => 'handleStored',
    ];

    public function render()
    {
        return view('livewire.users-index', [
            'users' => $this->search === null?
                User::paginate($this->paginate):
                User::latest()->where('name', 'like', '%' . $this->search . '%')->paginate($this->paginate)

        ]);
    }

    public function handleStored($user){
        // dd($user);
        session()->flash('success', "User $user[name] was created ");
        $this->render();
    }
}
