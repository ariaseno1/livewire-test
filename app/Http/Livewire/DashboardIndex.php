<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;

class DashboardIndex extends Component
{
    public $userId;

    public function mount(){
        $this->userId = User::find(1);
    }
    public function render()
    {
        return view('livewire.dashboard-index');
    }
}
