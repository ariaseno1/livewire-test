<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\User;
use Illuminate\Auth\Events\Validated;

class CreateUser extends Component
{
    public $name;
    public $posisi;
    public $username;
    public $email;
    public $password;
    public $is_admin;

    public function render()
    {
        return view('livewire.create-user');
    }

    public function store(){
        // $this->validated([
        //     'name'=>'required',
        //     'posisi'=>'required',
        //     'username'=>'required',
        //     'email'=>'required',
        //     'password'=>'required'
        // ]);
        $user = User::create([
            'name'=>$this->name,
            'posisi'=>$this->posisi,
            'username'=>$this->username,
            'email'=>$this->email,
            'password'=>bcrypt($this->password),
            'is_admin'=>$this->is_admin === null? 0:1
        ]);
        $this->clear();
        $this->emit('userCreated', $user);
    }

    public function clear(){
        $this->name = null;
        $this->posisi = null;
        $this->username = null;
        $this->email = null;
        $this->password = null;
        $this->is_admin = null;
    }
}
