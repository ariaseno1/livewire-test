<?php

namespace App\Http\Livewire;

use App\Models\User;
use Livewire\Component;

class MyProfile extends Component
{
    public $user;
    public $userId;

    public function mount(){
        $this->userId = User::find(1);
    }

    public function render()
    {

        return view('livewire.my-profile');
    }
}
